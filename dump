#!/bin/bash

# halt on errors
set -e;
set -u;

# determine root of repo
ROOT=$(cd $(dirname ${0})/.. 2>/dev/null && pwd -P);
cd ${ROOT};

function usage {
  echo "Clone database and files for local development";
  echo "";
  echo "Usage: bin/dump <sql|files|files-all|all> <dev|stage|prod>, default dev";
  echo "";
  exit 1;
}

# get the command and run it
COMMAND="${1:-}";
ENVIRONMENT="${2:-dev}";

# set environment variables
set -a; source ${ROOT}/.env; set +a;

case "${ENVIRONMENT}" in
  dev) REMOTE_HOST=${DEV_SERVER%% *} ;;
  stage) REMOTE_HOST=${STAGE_SERVER%% *} ;;
  prod) REMOTE_HOST=${PROD_SERVER%% *} ;;
  *) usage ;;
esac;

case "${COMMAND}" in
  sql)
    rm -rf ${RELATIVE_SQLDUMP_SRC} ${RELATIVE_SQLDATA_SRC};
    mkdir -pv ${RELATIVE_SQLDUMP_SRC};

    SQLDUMP_FILE=${RELATIVE_SQLDUMP_SRC}/${ENVIRONMENT}.${DEPLOY_INSTANCE}.${PROJECT_NAME}.sql.gz;
    echo "Dumping database from ${ENVIRONMENT}.${DEPLOY_INSTANCE}.${PROJECT_NAME}...";
    ssh -tt ${SSH_OWNER}@${REMOTE_HOST} "cd ${VM_CORE_PATH}; vendor/bin/drush sql-dump" | gzip > ${SQLDUMP_FILE};

    echo "Database dumped to ${SQLDUMP_FILE}";
    ;;

  files-all)
    mkdir -pv ${RELATIVE_FILES_SRC} ${RELATIVE_PRIVATE_SRC};

    echo "Dumping files from ${ENVIRONMENT}.${DEPLOY_INSTANCE}.${PROJECT_NAME}...";
    rsync -auv --delete --ignore-errors \
      ${SSH_APACHE}@${REMOTE_HOST}:${VM_CORE_PATH}/${RELATIVE_FILES_SRC}/. ${RELATIVE_FILES_SRC}/;
    rsync -auv --delete --ignore-errors \
      ${SSH_APACHE}@${REMOTE_HOST}:${VM_CORE_PATH}/${RELATIVE_PRIVATE_SRC}/. ${RELATIVE_PRIVATE_SRC}/;

    echo "Files dumped to ${RELATIVE_FILES_SRC} and ${RELATIVE_PRIVATE_SRC}";
    ;;

  files)
    mkdir -pv ${RELATIVE_FILES_SRC} ${RELATIVE_PRIVATE_SRC};

    echo "Dumping essential files from ${ENVIRONMENT}.${DEPLOY_INSTANCE}.${PROJECT_NAME}...";
    rsync -auv --delete --ignore-errors \
      --exclude=styles \
      --exclude=*.old \
      --exclude=*.bak \
      --exclude=*.pdf \
      --exclude=*.tar* \
      --exclude=*.zip \
      --exclude=*.doc* \
      --exclude=*.ppt* \
      --exclude=*.xls* \
      ${SSH_APACHE}@${REMOTE_HOST}:${VM_CORE_PATH}/${RELATIVE_FILES_SRC}/. ${RELATIVE_FILES_SRC}/;
    rsync -auv --delete --ignore-errors \
      --exclude=styles \
      --exclude=*.old \
      --exclude=*.bak \
      --exclude=*.pdf \
      --exclude=*.tar* \
      --exclude=*.zip \
      --exclude=*.doc* \
      --exclude=*.ppt* \
      --exclude=*.xls* \
      ${SSH_APACHE}@${REMOTE_HOST}:${VM_CORE_PATH}/${RELATIVE_PRIVATE_SRC}/. ${RELATIVE_PRIVATE_SRC}/;

    echo "Files dumped to ${RELATIVE_FILES_SRC} and ${RELATIVE_PRIVATE_SRC}";
    ;;

  all)
    ${ROOT}/bin/dump sql ${2:-};
    ${ROOT}/bin/dump files ${2:-};
    ;;

  *)
    usage
    ;;
esac;
